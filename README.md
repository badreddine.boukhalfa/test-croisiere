installation 

étape 1 :

cloner le projet :  

    https://gitlab.com/badreddine.boukhalfa/test-croisiere.git

etape 2 : 
    
    composer install 

etape 3 : 

    npm install 


etape 4 : 

creation de la base donnée: 

    vérifier le fichier .env

lancer la migration: 
    
    php artisan migrate

etape 5 : 

    php artisan serve
    npm run dev
    
