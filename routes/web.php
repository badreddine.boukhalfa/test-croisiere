<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/films','FilmsController@index');
Route::post('/film/add','FilmsController@store');
Route::get('/film/{id}','FilmsController@show');
Route::put('/film/edit/{id}','FilmsController@update');
Route::delete('/film/delete/{id}','FilmsController@destroy');

Route::get('/series','SeriesController@index');
Route::post('/serie/add','SeriesController@store');
Route::get('/serie/{id}','SeriesController@show');
Route::put('/serie/edit/{id}','SeriesController@update');
Route::delete('/serie/delete/{id}','SeriesController@destroy');

Route::get('/animes','AnimesController@index');
Route::post('/anime/add','AnimesController@store');
Route::get('/anime/{id}','AnimesController@show');
Route::put('/anime/edit/{id}','AnimesController@update');
Route::delete('/anime/delete/{id}','AnimesController@destroy');

