/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
import VueRouter from 'vue-router'
Vue.use(VueRouter);
Vue.component('add-film',require('./components/films/AddFilmComponent.vue').default);
Vue.component('add-serie',require('./components/series/AddSerieComponent.vue').default);
Vue.component('add-anime',require('./components/animes/AddAnimeComponent.vue').default);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import Home from './components/HomeComponent.vue';
import Films from './components/films/FilmsComponent.vue';
import FilmEdit from './components/films/EditFilmComponent.vue';
import Series from './components/series/SeriesComponent.vue';
import SerieEdit from './components/series/EditSerieComponent.vue';
import Animes from './components/animes/AnimesComponent.vue';
import AnimeEdit from './components/animes/EditAnimeComponent.vue';



const routes = [
    { path: '/', name: 'home',component: Home },
    { path: '/films', name: 'films',component: Films },
    { path: '/film/edit/:id', name: 'editfilm',component: FilmEdit },
    { path: '/series', name: 'series',component: Series },
    { path: '/serie/edit/:id', name: 'editserie',component: SerieEdit },
    { path: '/animes', name: 'animes',component: Animes },
    { path: '/serie/edit/:id', name: 'editanime',component: AnimeEdit }
  ]

const router = new VueRouter({
    routes // short for `routes: routes`
})

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router: router
});
